class audioplay {
    constructor(domElement, controls) {
        this.domElement = domElement;
        this.src = this.domElement.dataset.src;
        this.audio = new Audio(this.src);
        this.controls = controls;
        this.progress = this.domElement.querySelector(".monitor2 .progress");
        this.initcontrols();
        this.audio.ontimeupdate = () => { this.updateUI(); }
        this.initprogresaction();
    }
    initcontrols() {
        if (this.controls.play) {
            this.initplay(this.controls.play);
        }
        if (this.controls.left) {
            this.updateUIleft(this.controls.left);
        }
        if (this.controls.right) {
            this.updateUIright(this.controls.right);
        }
        if (this.controls.up) {
            this.updateUIup(this.controls.up);
        }
        if (this.controls.down) {
            this.updateUIdown(this.controls.down);
        }
    }
    initplay(domelement) {
        domelement.onclick = () => {
            const icon = domelement.querySelector("i");
            const ispause = icon.classList.contains("fa-play");
            if (!ispause) {
                icon.classList.replace('fa-pause', 'fa-play');
                this.pause();
            } else {
                icon.classList.replace('fa-play', 'fa-pause');
                this.play();
            }
        }
    }
    play() {
        this.audio.play().then().catch(err => console.log('Error al reproducir el archivo: ${err}'));
    }
    pause() {
        this.audio.pause();
    }
    updateUI() {
        const total = this.audio.duration;
        const current = this.audio.currentTime;
        const progress = (current / total) * 100;
        this.progress.style.width = progress + "%";
        const time = this.domElement.querySelector(".monitor2 .time");
        const minC = parseInt(current) / 60;
        const segC = parseInt(current) - (parseInt(minC) * 60);
        time.innerHTML = parseInt(minC) + ":" + parseInt(segC);
    }
    updateUIleft(domelement) {
        domelement.onclick = () => {
            this.audio.currentTime -= 5;
        }
    }
    updateUIup(domelement) {
        domelement.onclick = () => {
            this.pause();
            this.audio.currentTime = 0;
            if (this.domElement.dataset.src != './media/monster.mp3') {
                this.domElement.dataset.src = './media/monster.mp3';
                this.audio = new Audio('./media/monster.mp3');
            } else {
                this.domElement.dataset.src = './media/hero.mp3';
                this.audio = new Audio('./media/hero.mp3');
            }
            const icon = document.querySelector(".cuerpo .controles .botones .AB .a i");
            const ispause = icon.classList.contains("fa-pause");
            if (ispause) {
                icon.classList.replace('fa-pause', 'fa-play');
            }
        }
    }
    updateUIdown(domelement) {
        domelement.onclick = () => {
            this.pause();
            this.audio.currentTime = 0;
            if (this.domElement.dataset.src != './media/monster.mp3') {
                this.domElement.dataset.src = './media/monster.mp3';
                this.audio = new Audio('./media/monster.mp3');
            } else {
                this.domElement.dataset.src = './media/hero.mp3';
                this.audio = new Audio('./media/hero.mp3');
            }
            const icon = document.querySelector(".cuerpo .controles .botones .AB .a i");
            const ispause = icon.classList.contains("fa-pause");
            if (ispause) {
                icon.classList.replace('fa-pause', 'fa-play');
            }
        }
    }
    updateUIright(domelement) {
        domelement.onclick = () => {
            this.audio.currentTime += 5;
        }
    }
    initprogresaction() {
        const cover = this.domElement.querySelector(".monitor2");
        cover.onclick = (e) => {
            const x = e.offsetX;
            const totalx = cover.clientWidth;
            const progress = x / totalx;
            this.setCurrentTime(progress);
        }
    }
    setCurrentTime(progress) {
        this.audio.currentTime = this.audio.duration * progress;
    }
}