const playbtn = document.querySelector(".cuerpo .controles .botones .AB .a");
const leftbtn = document.querySelector(".cuerpo .controles .botones .cruceta .teclaleft");
const rightbtn = document.querySelector(".cuerpo .controles .botones .cruceta .teclaright");
const upbtn = document.querySelector(".cuerpo .controles .botones .cruceta .teclaup");
const downbtn = document.querySelector(".cuerpo .controles .botones .cruceta .tecladown");

const controls = {
    play: playbtn,
    left: leftbtn,
    right: rightbtn,
    up: upbtn,
    down: downbtn
}
const play = new audioplay(document.querySelector(".cuerpo .pantalla .monitor"), controls);